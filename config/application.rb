require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Japaneasy
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    
    # HG: default seems to be UTC, but set it anyway.
    config.time_zone = "UTC"
    #config.time_zone = "Asia/Tokyo"
    
    
    # HG: Move these statements to environments/production.rb and 
    #     environments/development.rb if you need to have different setups
    # HG: configure gmail SMTP mailer. NOTE: this is apparently limited to 99 mails per day.
    #     If japaneasy takes off, will need to self-host an smtp server or pay for a service.
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address:              "smtp.gmail.com",
      user_name:            "mailer.japaneasy.me@gmail.com",
      password:             "japaneasy42",
      port:                 587,
      authentication:       :plain,
      enable_starttls_auto: true }
  end
end
