Rails.application.routes.draw do

  # run 'rails routes' to see what rails thinks are the routes

  root 'static_pages#a1_top'

  # you can refer to the url '/blah' in code using the method blah_path,
  # without worrying about the actual controller/action names, which are
  # defined here:
  get '/company', to: 'static_pages#b1_company'
  get '/service', to: 'static_pages#b2_service'
  get '/recruit', to: 'static_pages#b3_recruit'
  get '/terms',   to: 'static_pages#b4_terms'
  get '/privacy', to: 'static_pages#b5_privacy'
  
  get '/signup',  to: 'users#new'
  
  # get the full RESTful paths.
  resources :users
  resources :teachers
  resources :lessons
  
  get '/students', to: 'users#index_students'
  post '/buy',     to: 'users#buy'
  post '/book',    to: 'lessons#book'

  # for sessions, we don't want the entire RESTful resources, just a few
  get     '/login',   to: 'sessions#new'
  post    '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
