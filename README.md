# Japaneasy README

japaneasy.me is a Ruby on Rails web app.

* Hosted on gitlab.com (`git clone git@gitlab.com:herminio/japaneasy.git`)
* Ruby 2.3.1
* Rails 5.0.0.1
* Static pages are handled by the static_pages controller

## For the designer

There is a `custom.scss` file in `app/assets/stylesheets`. Please add your SASS 
code in there. If you want to create multiple .scss files, you can add them all 
into that directory.

For Javascript, you can create your `.js` files in `app/assets/javascripts. 
It is possible to use CoffeeScript too, just name your file `.coffee`.

Add images into `app/assets/images`.

## Production server

* Amazon AWS instance running Ubuntu 14.04 server LTS
* Nginx web server
* Phusion Passenger app server
* DNS for japaneasy.me handled by hover.com

### Deploying a new version

Log into server and run these commands when a new version is ready for release:
```
1. cd /var/www/japaneasy/code
2. git pull
3. bundle install --deployment --without development test
4. bundle exec rake assets:precompile db:migrate RAILS_ENV=production
5. passenger-config restart-app $(pwd)
```

### Vagrant VM (local test server)

1. Download and install vagrant from <https://www.vagrantup.com/downloads.html>
2. Download and install VirtualBox from <https://www.virtualbox.org/wiki/Downloads>
3. Clone or pull the japaneasy project from gitlab into your PC to make sure you're up to date.
4. Open a terminal window on your PC and cd into your japaneasy project on your machine.
5. Check that the file called Vagrantfile is in your project.
6. Run the command: *vagrant up*. This will download and install a VirtualBox VM with a japaneasy test server running inside it. It can take a **long** time, so just leave it running.
7. When it has finished, you should see the message *"Rails 5.0.0.1 application starting in development on http://0.0.0.0:3000"*.
8. Connect to your private japaneasy server by opening a browser and going to <http://127.0.0.1:3000>.

Here are some handy commands to control the server (you must run them from the japaneasy project directory):

**vagrant up**: start up the VM  
**vagrant halt**: shut down the VM  
**vagrant help**: more commands

Any changes you make to the project files in your japaneasy project folder immediately are made to the server. You do not need to copy anything over. So for example, you can change some HTML in the project source, and then if you just refresh your browser page, you should see the change.

### BUGS
* BUG1 (fixed): Log in as admin, click create teacher. Enter mismatched passwords, then fix the error and successfully create a new teacher. That teacher is created as if s/he were a new student.
* BUG2 (fixed): Log in as a teacher, edit own profile, enter a password mismatch --> nil access error.
* BUG3 (fixed): Gravatar doesn't appear on production website. (Fix: disable ghostery)

### TODO
1. ~~(done): Create a Time Zone setting for all users (students, teachers, admins)~~
1. ~~(done): When teachers create lessons, the time is entered in the teacher's local time zone, but stored in UTC on db.~~
1. ~~(done): Teacher or admin can delete lesson.~~
1. ~~(done): Disallow creating lessons in the past~~
1. ~~(done): Disallow creating a lesson that overlaps with another lesson for the same teacher.~~
1. ~~(done): Students can request/book a lesson.~~
1. Add a better UI for creating lessons. It should be easy to select a date and time, and it should be easy to create weekly repeating lessons.
1. Improve website overall layout and design.
1. Localize and translate strings into Japanese.
1. Get a paypal account.
1. Students can buy credits via Paypal.
1. Set up and operate a customer service email account, e.g. help.japaneasy.me@gmail.com
1. When student has no credits and tries to book, offer a link to buy credits. Make it easy for student to buy credits.
1. Lessons starting in the next 2 hours can't be booked.
1. Think of some way to notify students when lesson is about to start (SMS? skype message?)
1. If student has problem with lesson (e.g. teacher didn't show up), there must be a way to raise an issue (complain).
1. Both teachers and students should have some way of giving feedback after the lesson. What form should feedback have? free text? choice from a list?
1. Create development website on AWS server e.g. japaneasy.me:3000. when people push to gitlab, changes should appear on dev website.
1. Set up SSL cert for https (letsencrypt)
1. Credits expire after X months 
  * HG thinks this is not feasible. Does every single credit come with an expiry date? What if you have a mixture of credits with different expiries, e.g. 5000 expire in one month, 3000 expire in two weeks. Is user expected to keep track of that?.
1. Add Lesson type ('daily', 'jlpt', 'business')

