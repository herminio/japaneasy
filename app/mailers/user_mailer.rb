class UserMailer < ApplicationMailer
  default from: 'mailer.japaneasy.me@google.com'
  
  # returns an ActionMailer::MessageDelivery object.
  def notify_student_lesson_booked(student, teacher, lesson)
    
    # instance vars will be available within the mail view
    @student = student
    @lesson = lesson
    
    addr_with_name = "\"#{@student.name}\" <#{@student.email}>"
    #addr_with_name = "\"#{@student.name}\" <hermi362@hotmail.com>" # FOR TESTING 
    mail(to: addr_with_name, subject: "You have booked a lesson")
  end
  
  # returns an ActionMailer::MessageDelivery object
  def notify_teacher_lesson_booked(student, teacher, lesson)
    
    # instance vars will be available within the mail view
    @student = student
    @teacher = teacher
    @lesson = lesson
    
    addr_with_name = "\"#{@teacher.name}\" <#{@teacher.email}>"
    #addr_with_name = "\"#{@teacher.name}\" <hermi362@hotmail.com>" # FOR TESTING 
    mail(to: addr_with_name, subject: "Class booking notification")
  end
end
