class LessonOverlapValidator < ActiveModel::Validator
  
  # iterate over the existsing lessons of the teacher. If any existing lesson overlaps this lesson, fail validation.
  def validate(record)
    
    # TODO: optimization: preprocess db query to exclude any lessons for this teacher that start earlier than ten hours or end later than ten hours, with respect to the start time of this record. 
    
    # for some reason, teacher.lessons does not produce any records! Had to use Lesson.where()
    #record.teacher.lessons.each do |lesson|
    Lesson.where(teacher_id: record.teacher.id).each do |lesson|
      
      # do not check for overlap against yourself (obviously)
      if record.id == lesson.id then 
        next 
      end
      
      unless validate_overlap(lesson, record)
        record.errors[:base] << "lesson time overlaps with another lesson"
        puts "!!! overlap validation failed!"
        return
      end
    end
  end
  
  def validate_overlap(a, b)
    return (a.starts_at + a.duration.minutes <= b.starts_at)  ||
           (b.starts_at + b.duration.minutes <= a.starts_at)
  end
end
