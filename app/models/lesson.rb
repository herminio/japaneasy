class Lesson < ApplicationRecord
  belongs_to :teacher, class_name: "User", foreign_key: "teacher_id"
  has_many :bookings, dependent: :destroy
  
  # a class should have at least one student
  validates :capacity, numericality: {greater_than_or_equal_to: 1}
  
  # class should last at least 10 mins
  # note: if you change this value, be sure to update the tests in lesson_test.rb
  validates :duration, numericality: {greater_than_or_equal_to: 10}
  
  validates :teacher, :starts_at, presence: true
  
  validate :lesson_cannot_be_in_the_past
  
  #include ActiveModel::Validations
  validates_with LessonOverlapValidator, if: :teacher
  
  # returns number of students who booked the class
  def num_bookings
    return self.bookings.count
  end
  
  # returns true if class is fully booked
  def full?
    return self.bookings.count >= self.capacity
  end
  
  def lesson_cannot_be_in_the_past
    if starts_at.present? && starts_at < Time.current
      errors.add(:base, "Lesson cannot start in the past")
    end
  end
end

