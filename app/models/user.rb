class User < ApplicationRecord
  attr_accessor :remember_token  # does not exist in database
  has_one :teacher_info
  has_many :lessons, class_name: "Lesson", foreign_key: "teacher_id"
  has_many :bookings, dependent: :destroy
  
  # this allows me to save teacher_info through this class.
  #accepts_nested_attributes_for :teacher_info
  
  # store all emails in lower case
  before_save { email.downcase! }
  
  validates :name, presence: true, length: { maximum: 50 }
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  
  validates :credits, numericality: true

  # Most of the secure password machinery is implemented using this single Rails method 
  has_secure_password
  
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  
  validates :time_zone, presence: true, allow_nil: false
  
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token. Used for cookie authentication and password resets.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # arrange to remember a user the next time they log in
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

end

