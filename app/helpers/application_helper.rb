module ApplicationHelper
  
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Japaneasy.me"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
    
  def parse_youtube url
    return if url.nil?
    
    u = URI.parse url
    if u.path =~ /watch/
      p CGI::parse(u.query)["v"].first
    else
      p u.path
    end
  end
    
end
