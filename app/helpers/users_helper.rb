module UsersHelper
  
  # Returns the Gravatar for the given user.
  def gravatar_for(user, options = { size: 80 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]

    # url-encoded path to public default avatar
    #default = 'http%3A%2F%2Fjapaneasy.me%2Fimages%2Favi-default-160px.png'
    # a geometric shape that is unique to the email hash
    #default = "identicon"
    # mm: mystery man
    default = "mm"

    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}&d=#{default}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
  
end
