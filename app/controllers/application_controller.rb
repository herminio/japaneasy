class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # This puts :en or :ja into every url you ask rails to generate for you
  def default_url_options
    { locale: I18n.locale }
  end

  before_action :set_locale
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  # make this helper class available in all controllers and views
  # we need it for logging a user in
  include SessionsHelper
  
  
  # my attempt at timezone support
  # source: https://www.varvet.com/blog/working-with-time-zones-in-ruby-on-rails/
  # a before_filter would also work, but apparently around_action is better
  around_action :user_time_zone, if: :current_user
  
  def user_time_zone(&block)
    Time.use_zone(current_user.time_zone, &block)
  end

end
