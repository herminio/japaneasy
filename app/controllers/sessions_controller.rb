class SessionsController < ApplicationController
  
  # display login form
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in user
      
      # handle the 'remember me' checkbox
      if params[:session][:remember_me] == '1'
        remember(user)
      else
        forget(user)
      end
      
      # students go to student page, teachers to go teacher page
      destination = user if user.student? || user.admin?
      destination = teacher_url(user.id) if user.teacher?
      redirect_back_or destination
    else
      # Create an error message.
      # just using flash is not good, b/c message persists until next request, which is not what we want in this case. The one we want to use is flash.now.
      #flash[:danger] = 'Invalid email/password combination'
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
