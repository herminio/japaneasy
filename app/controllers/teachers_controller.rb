class TeachersController < ApplicationController
  before_action :logged_in_user,  only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user,    only: [:edit, :update]
  before_action :admin_user,      only: [:new, :create, :destroy]

  def index
    @teachers = User.where(teacher: true)
  end
  
  def show
    @user = User.find(params[:id])
    @teacher_info = @user.teacher_info
    @lessons = @user.lessons.where("starts_at > :now", {now: Time.current})
  end
  

  def new
    @user = User.new
    @user.time_zone = nil
    @teacher_info = TeacherInfo.new
    @form_url = teachers_path
  end
  
  def create
    @user = User.new(user_params)
    @teacher_info = TeacherInfo.new(teacher_info_params)
    
    @user.teacher_info = @teacher_info
    @user.credits = 0
    @user.teacher = true  # flag new user as a teacher
    
    # save user and teacher_info to db (note: only need to save user: the association will handle saving teacherinfo, and giving it the correct user_id)
    if @user.save
      flash[:success] = "Teacher created."
      redirect_to teachers_url
    else
      @form_url = teachers_path
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
    @teacher_info = @user.teacher_info
    @form_url = teacher_path
  end
  
  def update
    
    # very nasty code. Need to tidy this up!
    if @user.update_attributes(user_params) && 
        @user.teacher_info.update_attributes(teacher_info_params)
      # Handle a successful update
      flash[:success] = "Profile updated"
      redirect_to teacher_url(@user)
    else
      @teacher_info = @user.teacher_info
      @form_url = teacher_path
      render 'edit'
    end
    
  end
  
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Teacher deleted"
    redirect_to teachers_url
  end
  
  private
  
  def user_params
    params.require(:user).permit(:name, :email, 
    :password, :password_confirmation, :time_zone)
  end
  def teacher_info_params
    params.require(:teacher_info).permit(:profile, :message, :yt_vid_1, :yt_vid_2, :yt_vid_3)
  end
  
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end
  
  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user) || current_user.admin?
  end
  
  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
    
  
end
