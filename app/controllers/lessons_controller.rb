class LessonsController < ApplicationController

  # display new lesson form
  def new
    @lesson = Lesson.new
    @lesson.teacher_id = params[:teacher_id]
  end
  
  def create
    @lesson = Lesson.new
    @lesson.teacher_id = params[:lesson][:teacher_id]
    @lesson.capacity = params[:lesson][:capacity]
    @lesson.duration = params[:lesson][:duration]
    @lesson.starts_at = Time.zone.local(params[:lesson]["starts_at(1i)"],
                                        params[:lesson]["starts_at(2i)"],
                                        params[:lesson]["starts_at(3i)"],
                                        params[:lesson]["starts_at(4i)"],
                                        params[:lesson]["starts_at(5i)"],
                                        0)
                                                                                    
    if @lesson.save
      flash[:success] = "Lesson created"
      redirect_to teacher_url(@lesson.teacher)
    else      
      render 'new'
    end
  end
  
  def destroy
    cached_teacher_url = teacher_url(Lesson.find(params[:id]).teacher)
    Lesson.find(params[:id]).destroy
    flash[:success] = "Lesson deleted"
    redirect_to cached_teacher_url
    
  end
  
  def book
    lesson = Lesson.find(params[:lesson])

    if (! current_user.student?)
      flash[:danger] = "You must be a student to book a lesson."
      redirect_to teacher_url(lesson.teacher)
      return
    end
    if (current_user.credits == 0)
      flash[:danger] = "Sorry, you have no credits."
      redirect_to teacher_url(lesson.teacher)
      return
    end
    if (lesson.full?)
      flash[:danger] = "Sorry, class is full"
      redirect_to teacher_url(lesson.teacher)
      return
    end
    if (Booking.where(lesson: lesson, user: current_user).any?)
      flash[:danger] = "You already booked that class"
      redirect_to teacher_url(lesson.teacher)
      return
    end
    
    
    Booking.create(lesson: lesson, user: current_user)
    
    # deduct one credit from student
    current_user.credits   -= 1
    current_user.save!
    
    # add one credit to teacher
    lesson.teacher.credits += 1
    lesson.teacher.save!
    
    # send email to both student and teacher
    UserMailer.notify_student_lesson_booked(current_user, lesson.teacher, lesson).deliver_later
    UserMailer.notify_teacher_lesson_booked(current_user, lesson.teacher, lesson).deliver_later
    
    flash[:success] = "Lesson booked. Check your email. Please make sure you're on time for your lesson."
    redirect_to teacher_url(lesson.teacher)
  end
  
  
end
