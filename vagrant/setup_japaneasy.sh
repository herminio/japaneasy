#!/bin/sh
# Original script on https://github.com/orendon/vagrant-rails

# load rbenv and shims
export RBENV_ROOT="${HOME}/.rbenv"
export PATH="${RBENV_ROOT}/bin:${PATH}"
export PATH="${RBENV_ROOT}/shims:${PATH}"

# setup japaneasy rails project
cd /vagrant
bundle install
rbenv rehash
sudo sudo -u postgres psql -1 -c "CREATE USER japaneasy WITH PASSWORD 'japaneasy';"
sudo sudo -u postgres psql -1 -c "ALTER USER japaneasy WITH SUPERUSER;"
rake db:create
rake db:migrate
rake db:seed
