#!/bin/sh

# load rbenv and shims
export RBENV_ROOT="${HOME}/.rbenv"
export PATH="${RBENV_ROOT}/bin:${PATH}"
export PATH="${RBENV_ROOT}/shims:${PATH}"

# run japaneasy rails server in puma
cd /vagrant
if [ ! -e tmp/pids/server.pid ]; then
  ./bin/rails server -b 0.0.0.0 -p 3000 --daemon
fi
