class AddYoutubeVidsToTeacherInfos < ActiveRecord::Migration[5.0]
  def change
    add_column :teacher_infos, :yt_vid_1, :string
    add_column :teacher_infos, :yt_vid_2, :string
    add_column :teacher_infos, :yt_vid_3, :string
  end
end
