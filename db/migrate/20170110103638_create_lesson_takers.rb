class CreateLessonTakers < ActiveRecord::Migration[5.0]
  def change
    create_table :lesson_takers do |t|
      t.references :lesson, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
