class CreateTeacherInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :teacher_infos do |t|
      t.integer :user_id
      t.text :profile
      t.text :message

      t.timestamps
    end
  end
end
