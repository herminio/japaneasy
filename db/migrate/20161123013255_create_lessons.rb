class CreateLessons < ActiveRecord::Migration[5.0]
  def change
    create_table :lessons do |t|
      t.integer :capacity,    default: 1
      t.datetime :starts_at
      t.integer :duration,    default: 45
      t.integer :teacher_id
      
      t.index ["teacher_id", "starts_at"], name: "index_teacher_id_starts_at", unique: true, using: :btree
    end
  end
end
