class RenameLessonTakersToBookings < ActiveRecord::Migration[5.0]
  def change
    rename_table :lesson_takers, :bookings
  end
end
