# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# reset database with 'rails db:migrate:reset'
# populate new blank db with 'rails db:seed'


User.create!(name:  "Herminio Gonzalez",
             email: "herminio@gmail.com",
             password:              "password",
             password_confirmation: "password",
             credits: 0,
             admin: true,
             time_zone: "Tokyo")
             
User.create!(name:  "Shuji Iwata",
            email: "amagaeru.0403@gmail.com",
            password:              "password",
            password_confirmation: "password",
            credits: 0,
            admin: true,
            time_zone: "Tokyo")

User.create!(name:  "Alan Administrator",
             email: "admin1@example.com",
             password:              "password",
             password_confirmation: "password",
             credits: 0,
             admin: true,
             time_zone: "Tokyo")

5.times do |n|
  name  = Faker::Name.first_name + " Teacher"
  email = "teacher#{n+1}@example.com"
  password = "password"
  u = User.new(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              teacher: true,
              credits: 0,
              time_zone: "Tokyo")
  u.save!

  u.teacher_info = TeacherInfo.new(
    profile: "<strong>Education</strong>
    <ul>
    <li><a href=\"http://www.ox.ac.uk\" target=\"blank\">#{Faker::Educator.university}</a></li>
      <ul>
      <li>#{Faker::Educator.course}</li>
      </ul>
    <li>#{Faker::Educator.university}</li>
      <ul>
      <li>#{Faker::Educator.course}</li>
      </ul>
    </ul>",
    message: "<p>#{Faker::Lorem.sentence}</p>
    <p>#{Faker::Lorem.sentence}
    <em>#{Faker::Lorem.sentence}<em></p>
    <p><strong>Skype handle: #{email}</strong></p>")
    
    Lesson.new(teacher_id: u.id, capacity: 1, starts_at: "2018-04-01 00:00:00", duration: 45).save
    Lesson.new(teacher_id: u.id, capacity: 2, starts_at: "2018-04-02 00:00:00", duration: 45).save
    Lesson.new(teacher_id: u.id, capacity: 2, starts_at: "2018-04-02 01:00:00", duration: 30).save
  end


50.times do |n|
  name  = Faker::Name.first_name + " Student"
  email = "student#{n+1}@example.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               student: true,
               credits: 5,
               time_zone: "Hawaii")
end

