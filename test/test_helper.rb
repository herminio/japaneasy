ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  # Returns true if a test user is logged in.
  def is_logged_in?
    !session[:user_id].nil?
  end

  # Log in as a particular user.
  # def log_in_as(user)
  #   session[:user_id] = user.id
  # end



  
  # Hermi: returns something like "?locale=en"
  # use it for getting assert_select to work
  def locale_url
    "?locale=#{I18n.locale}"
  end

  # basically the same as asset_select, but tacking on the locale at the end of the path
  def assert_select_with_locale(pattern, url_path, options=nil)
    assert_select pattern, url_path + locale_url, options
  end

end


# here we're extending the class IntegrationTest by adding a new method to it
class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
end
