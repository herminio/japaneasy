require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @michael = users(:michael)  # michael is an administrator
    @archer  = users(:archer)   # archer is a teacher
    @malory  = users(:malory)   # a student
  end

  test "layout links" do
    get root_path
    assert_template 'static_pages/a1_top'
    assert_select_with_locale "a[href=?]", root_path
    assert_select_with_locale "a[href=?]", company_path
    assert_select_with_locale "a[href=?]", service_path
    assert_select_with_locale "a[href=?]", recruit_path
    assert_select_with_locale "a[href=?]", terms_path
    assert_select_with_locale "a[href=?]", privacy_path
  end

  test "layout links after logging in as admin" do
    log_in_as(@michael)
    get root_path
    
    assert_select_with_locale "a[href=?]", user_path(@michael)
    assert_select_with_locale "a[href=?]", teacher_path(@michael), false
    assert_select_with_locale "a[href=?]", students_path
    assert_select_with_locale "a[href=?]", logout_path
    assert_select_with_locale "a[href=?]", new_teacher_path
  end
    
  test "layout links after logging in as teacher" do
    log_in_as(@archer)
    get root_path
    
    assert_select_with_locale "a[href=?]", user_path(@archer), false
    assert_select_with_locale "a[href=?]", teacher_path(@archer)
    assert_select_with_locale "a[href=?]", students_path
    assert_select_with_locale "a[href=?]", logout_path
    assert_select_with_locale "a[href=?]", new_teacher_path, false
  end

  test "layout links after logging in as student" do
    log_in_as(@malory)
    get root_path
    
    assert_select_with_locale "a[href=?]", user_path(@malory)
    assert_select_with_locale "a[href=?]", teacher_path(@malory), false
    assert_select_with_locale "a[href=?]", students_path
    assert_select_with_locale "a[href=?]", logout_path
    assert_select_with_locale "a[href=?]", new_teacher_path, false
  end

end
