require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:michael)
    @student = users(:malory)
  end
  
  test "unsuccessful edit as admin" do
    log_in_as(@admin)
    get edit_user_path(@admin)
    assert_template 'users/edit'
    patch user_path(@admin), params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
                                      
    assert_template 'users/edit'
    assert_select "div.alert", text: "The form contains 4 errors."
  end

  test "unsuccessful edit as student" do
    log_in_as(@student)
    get edit_user_path(@student)
    assert_template 'users/edit'
    patch user_path(@student), params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }
                                      
    assert_template 'users/edit'
    assert_select "div.alert", text: "The form contains 4 errors."
  end

  test "successful edit with friendly forwarding" do
    # attempt to edit your profile before logging in
    get edit_user_path(@student)
    
    log_in_as(@student)
    
    # you should have been redirected to the edit profile page
    assert_redirected_to edit_user_path(@student)
    
    # make sure that on next login, you won't go to the edit profile page
    # value should have been deleted
    assert session[:forwarding_url].nil?
    
    # update your profile
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@student), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }

    # there should be some notification like 'profile updated'.
    assert_not flash.empty?
    # you should be redirected to the view profile page
    assert_redirected_to @student
    
    # check that the name and email have been updated.
    @student.reload
    assert_equal name,  @student.name
    assert_equal email, @student.email
  end


end
