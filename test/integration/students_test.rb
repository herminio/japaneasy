require 'test_helper'

class TeachersTest < ActionDispatch::IntegrationTest
  
  
  def setup
    @student = users(:malory)
  end
  
  test "see studnet page after logging in" do
    log_in_as(@student)
    follow_redirect!
    assert_template 'users/show'

    assert_select "a[href=?]", edit_user_path(@student)+locale_url, count: 1
  end
  
  test "students should not see the give credit buttons" do
    log_in_as(@student)
    follow_redirect!
    
    assert_select "form[action*=buy]", {count: 0}, "Students should not see any buttons for giving oneself credits!"
  end
  
end
