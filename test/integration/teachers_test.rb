require 'test_helper'

class TeachersTest < ActionDispatch::IntegrationTest
  
  
  def setup
    @admin = users(:michael)
    @student = users(:malory)
    @teacher = users(:archer)
  end
  
  test "admin can create a new teacher" do
    log_in_as(@admin)
    get teachers_path
    assert_template 'teachers/index'
    assert_select "a[href=?]", new_teacher_path+locale_url
    
    @form_url = teachers_path
    get new_teacher_path
    assert_template 'teachers/new'
    
    
    assert_difference 'User.count + TeacherInfo.count', difference = 2, message = "number of users and number of teacher_infos should increment" do
      post teachers_path , 
        params: { user: {name: "teacher1", time_zone: "UTC", email: "teacher1@example.com", 
          password: "password", password_confirmation: "password"}, 
        teacher_info: {profile: "the profile", message: "the message",
          yt_vid_3: "https://www.youtube.com/watch?v=K3meJyiYWFw&t=2297s"}}
    end
    follow_redirect!
    assert_template 'teachers/index'
  end
  
  test "admin makes mistake creating teacher" do
    log_in_as(@admin)
    get new_teacher_path
    assert_template 'teachers/new'

    assert_difference 'User.count + TeacherInfo.count', difference = 0, message = "no new records created" do
      post teachers_path , 
        params: { user: {name: "teacher1", email: "teacher1@example.com", 
          password: "password", password_confirmation: "passwordxxx"}, 
        teacher_info: {profile: "the profile", message: "the message"}}
    end
    assert_template 'teachers/new'
    
    # after failing to create the teacher because of password mismatch, fix the problem and try again
    post teachers_path , 
      params: { user: {name: "teacher1", time_zone: "UTC", email: "teacher1@example.com", 
        password: "password", password_confirmation: "password"}, 
      teacher_info: {profile: "the profile", message: "the message"}}
    follow_redirect!
    assert_template 'teachers/index'
      
    
  end
  
  
  test "teacher cannot create a new teacher" do
    log_in_as(@teacher)
    
    get teachers_path
    assert_template 'teachers/index'
    assert_select "a[href=?]", new_teacher_path+locale_url, count: 0
  end
  
  test "student cannot create a new teacher" do
    log_in_as(@student)
    
    get teachers_path
    assert_template 'teachers/index'
    assert_select "a[href=?]", new_teacher_path+locale_url, count: 0
  end
  
  test "public visitor cannot create a new teacher" do    
    get teachers_path
    assert_template 'teachers/index'
    assert_select "a[href=?]", new_teacher_path+locale_url, count: 0
  end
  
  test "only admin can edit teacher" do
    log_in_as(@admin)
        
    # create a new teacher
    assert_difference 'User.count + TeacherInfo.count', difference = 2, message = "number of users and number of teacher_infos should increment" do
      post teachers_path , 
        params: { user: {name: "teacher2", time_zone: "UTC", email: "teacher2@example.com", password: "password", password_confirmation: "password"}, 
        teacher_info: {profile: "the profile", message: "the message"}}
    end
    new_teacher = User.find_by(email: "teacher2@example.com")
    assert_not new_teacher.nil?, "created a new teacher while logged in as admin"
    
    # as admin, try to edit this teacher
    new_name = "teacher2 2222"
    patch teacher_path(new_teacher), params: { user: {name: new_name, email: "teacher2@example.com"}, 
        teacher_info: {profile: "the profile 2", message: "the message 2"}}
    t = User.find(new_teacher.id)
    assert t.name==new_name , "edited a teacher while logged in as admin"
    
    # as this new teacher, try to edit self
    log_in_as(new_teacher)
    new_name = "teacher2 3333"
    patch teacher_path(new_teacher), params: { user: {name: new_name, email: "teacher2@example.com"}, 
        teacher_info: {profile: "the profile 3", message: "the message 3"}}
    t = User.find(new_teacher.id)
    assert t.name==new_name, "edited a teacher while logged in as that teacher"
    
    # as some other teacher, try to edit self
    another_teacher = users(:archer)
    log_in_as(another_teacher)
    new_name = "teacher2 44444"
    patch teacher_path(new_teacher), params: { user: {name: new_name, email: "teacher2@example.com"}, 
        teacher_info: {profile: "the profile 4", message: "the message 4"}}
    t = User.find(new_teacher.id)
    assert_not_equal t.name, new_name, "tried to edit a teacher while logged in as another teacher"
    
    # as a public visitor
    # perform log out
    delete logout_path
    new_name = "teacher2 55555"
    patch teacher_path(new_teacher), params: { user: {name: new_name, email: "teacher2@example.com"}, 
        teacher_info: {profile: "the profile 5", message: "the message 5"}}
    t = User.find(new_teacher.id)
    assert_not_equal t.name, new_name, "tried to edit a teacher while logged in as another teacher"
  end
  
  test "show teacher page after logging in" do
    log_in_as @teacher
    follow_redirect!
    assert_template 'teachers/show'
  end
  
  test "teachers should not see any give credit buttons" do
    log_in_as(@teacher)
    follow_redirect!
    
    assert_select "form[action*=buy]", {count: 0}, "Teachers should not see any buttons for giving oneself credits!"
  end
  
  
end
