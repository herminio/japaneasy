require 'test_helper'

class LessonsTest < ActionDispatch::IntegrationTest
  
  def setup
    @hill = users(:mr_hillbourne)
    @hill_private_lesson = lessons(:one_student)
    @hill_group_lesson = lessons(:two_students)
    @dora = users(:student_1)
    @athena = users(:student_2)
  end
  
  test "dora books a lesson" do
    log_in_as(@dora)
    get teacher_path(@hill)
    assert_template 'teachers/show'
    assert_select "input[value=?]", "Book lesson"
    dora_credits_before = @dora.credits
    hill_credits_before = @hill.credits

    assert_difference 'Booking.count', difference = 1, message = "Lesson was not booked" do
      post book_path(lesson: @hill_private_lesson)
    end
    
    @dora.reload
    assert_equal @dora.credits, (dora_credits_before - 1), "student credit has not been deducted"
    
    @hill.reload
    assert_equal @hill.credits, (hill_credits_before + 1), "teacher has not received credit"
    
    assert @hill_private_lesson.num_bookings == 1
    lt =  @hill_private_lesson.bookings.first
    assert lt.lesson == @hill_private_lesson
    assert lt.user == @dora
  end
  
  test "athena tries to book a fully booked lesson" do
    log_in_as(@dora)
    assert_difference 'Booking.count', difference = 1 do
      post book_path(lesson: @hill_private_lesson)
    end
    
    log_in_as(@athena)
    athena_credit_before = @athena.credits
    
    assert_difference 'Booking.count', difference = 0 do
      post book_path(lesson: @hill_private_lesson)
    end
    assert flash[:danger]
    
    @athena.reload
    assert_equal @athena.credits, athena_credit_before, "ahtena's credits should not change"
  end
  
  test "dora tries to book a lesson she already booked" do
    log_in_as(@dora)
    assert_difference 'Booking.count', difference = 1 do
      post book_path(lesson: @hill_group_lesson)
    end
    
    dora_credits_before = @dora.reload.credits
    
    assert_difference 'Booking.count', difference = 0 do
      post book_path(lesson: @hill_group_lesson)
    end
    
    assert_equal @dora.reload.credits, dora_credits_before, "dora's credits should not change"
    
    assert flash[:danger]
    
    
  end
end