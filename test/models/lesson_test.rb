require 'test_helper'

class LessonTest < ActiveSupport::TestCase
  # by default, rails loads all fixtures, so you don't need to explicitly state what fixtures you need unless you want to.
  #fixtures :lessons
  
  test "lesson teacher and start time must be present" do

    # a new lesson has default sane capacity and duration, but teacher and start time will be nil.
    lesson = Lesson.new
    
    assert lesson.invalid?
    assert lesson.errors[:teacher].any?
    assert lesson.errors[:starts_at].any?
  end
  
  test "duration must be at least 10 mins" do
    lesson = Lesson.new(teacher: nil, capacity: 2, starts_at: 1.week.from_now)
    lesson.duration = -1
    assert lesson.invalid?
    assert_equal [I18n.translate('errors.messages.greater_than_or_equal_to') % {count: 10}], 
      lesson.errors[:duration]
      
    lesson.duration = 0
    assert lesson.invalid?
    assert_equal [I18n.translate('errors.messages.greater_than_or_equal_to') % {count: 10}], 
      lesson.errors[:duration]    

    lesson.duration = 5
    assert lesson.invalid?
    assert_equal [I18n.translate('errors.messages.greater_than_or_equal_to') % {count: 10}], 
      lesson.errors[:duration]    
  end
  
  test "at least one student in lesson" do
    lesson = Lesson.new(teacher: nil, starts_at: 1.week.from_now, duration: 45)
    lesson.capacity = -1
    assert lesson.invalid?
    assert_equal [I18n.translate('errors.messages.greater_than_or_equal_to') % {count: 1}], 
      lesson.errors[:capacity]    

    lesson.capacity = 0
    assert lesson.invalid?
    assert_equal [I18n.translate('errors.messages.greater_than_or_equal_to') % {count: 1}], 
      lesson.errors[:capacity]    
  end
  
  test "teacher cannot have overlapping lessons" do
    # set up two overlapping lessons for same teacher
    teach = users(:nishizawa)
    start_time1 = "2018-01-10 00:00:00"
    start_time2 = "2018-01-10 00:20:00"
    
    # create the first lesson
    lesson1 = Lesson.new(teacher: teach, starts_at: start_time1, duration: 45)
    assert lesson1.save
    puts "---- saved lesson 1: #{lesson1.inspect}"
    
    # try to create second lesson (should fail)
    lesson2 = Lesson.new(teacher: teach, starts_at: start_time2, duration: 45)
    
    assert lesson1.valid?, "first lesson should be valid"
    assert lesson2.invalid?, "second lesson should overlap and fail"
    assert_equal ["lesson time overlaps with another lesson"], lesson2.errors[:base]
    
    # attempt to create another lesson. This one's start and end encompass the first lesson.
    start_time2 = "2018-01-09 23:50:00"
    lesson2 = Lesson.new(teacher: teach, starts_at: start_time2, duration: 120)
    assert lesson2.invalid?, "second lesson should overlap and fail"
    assert_equal ["lesson time overlaps with another lesson"], lesson2.errors[:base]
    
    # attempt to create another lesson. This one's start and end lie entirely within the first lesson.
    start_time2 = "2018-01-10 00:10:00"
    lesson2 = Lesson.new(teacher: teach, starts_at: start_time2, duration: 20)
    assert lesson2.invalid?, "second lesson should overlap and fail"
    assert_equal ["lesson time overlaps with another lesson"], lesson2.errors[:base]
    
    # attempt to create another lesson. This one starts when the first lesson ends, and should pass validation.
    start_time2 = "2018-01-10 00:45:00"
    lesson2 = Lesson.new(teacher: teach, starts_at: start_time2, duration: 45)
    assert lesson2.valid?, "second lesson should be valid"
    
    # attempt to create another lesson. This one starts after the first lesson ends, and should pass validation. It is the control experiment of lesson2s
    start_time2 = "2018-01-10 02:00:00"
    lesson2 = Lesson.new(teacher: teach, starts_at: start_time2, duration: 45)
    assert lesson2.valid?, "second lesson should be valid"
  end
  
  test "create lesson in the past" do
    [1.second.ago, 1.minute.ago, 1.week.ago, 1.month.ago, 1.year.ago, "2016-10-10 12:00:00"].each do |start_time|
      lesson = Lesson.new(teacher: users(:nishizawa), starts_at: start_time)
      assert lesson.invalid?, "lesson in the past should not be valid"
      assert_equal ["Lesson cannot start in the past"], lesson.errors[:base]
    end    
  end
  
end
