require 'test_helper'

class TeacherInfoTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  test "should not save teacher info without user_id" do
    ti = TeacherInfo.new
    assert_not ti.save, "Saved the teacher info without a user_id."
  end
  
end
