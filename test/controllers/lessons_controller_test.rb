require 'test_helper'

class LessonsControllerTest < ActionDispatch::IntegrationTest
  def setup 
    @teacher1 = users(:mr_hillbourne)
    @student1 = users(:student_1)
  end
    
  test "student_1 can view mr_hillbourne's lessons" do
    log_in_as(@student1)
    get teacher_path(@teacher1)
    assert flash.empty?
    assert_response :success
    assert_select "input[value=?]", "Book lesson"
  end
  
end
