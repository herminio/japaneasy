require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get root" do
    get root_url
    assert_response :success
  end

  test "should get b1_company" do
    get company_url
    assert_response :success
  end

  test "should get b2_service" do
    get service_url
    assert_response :success
  end

  test "should get b3_recruit" do
    get recruit_url
    assert_response :success
  end

  test "should get b4_terms" do
    get terms_url
    assert_response :success
  end

  test "should get b5_privacy" do
    get privacy_url
    assert_response :success
  end

end
